<?php
class LUXCOURIERGATEWAYTOOLS
{
	private $SHIPPINGMETHODCONECTIONS = array();

	
	
	// ============================================================================
	public function __construct()
	{

	}

	// ============================================================================
	public function GET_REQUEST($url, &$httpcode, &$message){
		if ($this->_isCurl())
		{
			$ch = curl_init($url); 
					
			curl_setopt($ch, CURLOPT_VERBOSE, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);

			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
			curl_setopt($ch, CURLOPT_TIMEOUT, 400);

			$result = curl_exec($ch);

			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);	
			
			curl_close($ch);
		} else
		{
			$result = file_get_contents($url);
			if($result === false){
				$error = error_get_last();
				$httpcode = 400;
				$message = $error['message'];
				return false;
			}
			$httpcode = 200;
		}
		
		return $result;
	}

	// ============================================================================
	public function POST_REQUEST($url, &$httpcode, &$message, $postdata = array(), $headers = array()){
		$postdata = http_build_query($postdata);
		$headers = (array)$headers;

		if ($this->_isCurl())
		{
			$ch = curl_init($url); 
				
			curl_setopt($ch, CURLOPT_VERBOSE, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);

			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
			
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
			curl_setopt($ch, CURLOPT_TIMEOUT, 400);

			$result = curl_exec($ch);

			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);	
			
			curl_close($ch);
		} else
		{
			$headers[] = 'Content-type: application/x-www-form-urlencoded';
			$options = array(
				'http' =>
					array(
						'method'  => 'POST',
						'header'  => $headers,
						'content' => $postdata 
					)
			);
			$streamContext  = stream_context_create($options);
			// error_log($url);
			$result = file_get_contents($url, false, $streamContext);
			if($result === false){
				$error = error_get_last();
				$httpcode = 400;
				$message = $error['message'];
				return false;
			}
			$httpcode = 200;
		}
		
		return $result;
	}
	
	// ============================================================================
	protected function _isCurl(){
		return function_exists('curl_version');
	}

	// ============================================================================
	public function checkcompanydetail($idshippingmethod)
	{
		if (isset($this->SHIPPINGMETHODCONECTIONS[$idshippingmethod])) return true;

		return $this->setcompanydetail($idshippingmethod);
	}
	
	// ============================================================================
	protected function setcompanydetail($idshippingmethod)
	{
		$arr = LUXCOURIERGATEWAY_PLUGINTOOLS::get_shippingmethodoptions($idshippingmethod);

		$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod] = $arr;

		$t = $this->setcompanyjwt($idshippingmethod);
		if (!$t) return false;

		$t = $this->setcompanysignin($idshippingmethod);
		if (!$t) return false;

		return true;
	}

	// ============================================================================
	protected function setcompanyjwt($idshippingmethod)
	{
		$url = $this->prepareurl($idshippingmethod, '/default/wse_getJWT');

		$response = $this->GET_REQUEST($url, $httpcode, $message);
		if (!$response) return false;

		$response = json_decode($response, true);

		if ($response['e']) return false;

		$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['jwt'] = $response['jwt'];

		return true;
	}
	
	// ============================================================================
	protected function setcompanysignin($idshippingmethod)
	{
		$url = $this->prepareurl($idshippingmethod, '/user/wse_signin');

		$headers = array();
		$headers[] = 'Authorization:'.$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['jwt'];

		$postdata = array (
			'identifier' => $this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['user']
			, 'password' => $this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['password']
		);

		$response = $this->POST_REQUEST($url, $httpcode, $message, $postdata, $headers);

		$response = json_decode($response, true);

		if ($response['e']) return false;
		
		return true;
	}

	// ============================================================================
	protected function prepareurl($idshippingmethod, $suffix = '')
	{
		$url = $this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['apiurl'];
		$url = rtrim($url, '/');
		$url .= $suffix;
		return $url;
	}
	
	// ============================================================================
	public function setclientpostdata($idshippingmethod, $postdata)
	{
		$clientfields = array(
			'client_name'
			, 'client_company'
			, 'client_site'
			, 'client_address'
			, 'client_phone'
			, 'client_contact'
		);

		foreach ($clientfields as $v)
		{
			if (!isset($postdata[$v])) $postdata[$v] = $this->SHIPPINGMETHODCONECTIONS[$idshippingmethod][$v];
		}

		return $postdata;
	}

	// ============================================================================
	public function getsignature($idshippingmethod, $prefix = '')
	{
		$str = $prefix.$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['secretkey'];
		
		return md5($str);
	}
	
	// ============================================================================
	public function cancelorder($idshippingmethod, $idorder)
	{
		$url = $this->prepareurl($idshippingmethod, '/sale/wse_client_cancelorder');

		$headers = array();
		$headers[] = 'Authorization:'.$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['jwt'];

		$postdata = array();
		$postdata['idorder'] = $idorder;
		$postdata['signature'] = $this->getsignature($idshippingmethod, $idorder);

		$response = $this->POST_REQUEST($url, $httpcode, $message, $postdata, $headers);
		
		$response = json_decode($response, true);

		if ($response['e']) return false;
		
		return $response;
	}
	
	// ============================================================================
	public function archiveorder($idshippingmethod, $idorder)
	{
		$url = $this->prepareurl($idshippingmethod, '/sale/wse_client_archiveorder');

		$headers = array();
		$headers[] = 'Authorization:'.$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['jwt'];

		$postdata = array();
		$postdata['idorder'] = $idorder;
		$postdata['signature'] = $this->getsignature($idshippingmethod, $idorder);

		$response = $this->POST_REQUEST($url, $httpcode, $message, $postdata, $headers);
		
		$response = json_decode($response, true);

		if ($response['e']) return false;
		
		return $response;
	}
	
	// ============================================================================
	public function savepreorder($idshippingmethod, $postdata)
	{
		$url = $this->prepareurl($idshippingmethod, '/sale/wse_savepreorder');

		$headers = array();
		$headers[] = 'Authorization:'.$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['jwt'];

		$postdata = $this->setclientpostdata($idshippingmethod, $postdata);

		$prefix = '';
		$prefix .= $postdata['titular_name'];
		$prefix .= $postdata['titular_phone'];
		$prefix .= $postdata['titular_email'];

		$postdata['signature'] = $this->getsignature($idshippingmethod, $prefix);

		$response = $this->POST_REQUEST($url, $httpcode, $message, $postdata, $headers);
		
		$response = json_decode($response, true);

		if ($response['e']) return false;
		
		return $response;
	}
	
	// ============================================================================
	public function confirmorder($idshippingmethod, $idorder)
	{
		$url = $this->prepareurl($idshippingmethod, '/sale/wse_client_confirmorder');

		$headers = array();
		$headers[] = 'Authorization:'.$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['jwt'];

		$postdata = array();
		$postdata['idorder'] = $idorder;
		$postdata['signature'] = $this->getsignature($idshippingmethod, $idorder);

		$response = $this->POST_REQUEST($url, $httpcode, $message, $postdata, $headers);
		
		$response = json_decode($response, true);

		if ($response['e']) return false;
		
		return $response;
	}
	
	// ============================================================================
	public function changeorder($idshippingmethod, $postdata)
	{
		$url = $this->prepareurl($idshippingmethod, '/sale/wse_changeorder');

		$headers = array();
		$headers[] = 'Authorization:'.$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['jwt'];

		$postdata = $this->setclientpostdata($idshippingmethod, $postdata);
		
		$prefix = '';
		$prefix .= $postdata['idorder'];

		$postdata['signature'] = $this->getsignature($idshippingmethod, $prefix);

		$response = $this->POST_REQUEST($url, $httpcode, $message, $postdata, $headers);

		// error_log($url);
		// echo '<pre>';
		// print_r($response);
		// echo '</pre>';

		
		$response = json_decode($response, true);

		if ($response['e']) return false;
		
		return $response;
	}
	
	// ============================================================================
	public function readyfortake($idshippingmethod, $idorder)
	{
		$url = $this->prepareurl($idshippingmethod, '/sale/wse_client_readyfortake');

		$headers = array();
		$headers[] = 'Authorization:'.$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['jwt'];

		$postdata = array();
		$postdata['idorder'] = $idorder;
		$postdata['signature'] = $this->getsignature($idshippingmethod, $idorder);

		$response = $this->POST_REQUEST($url, $httpcode, $message, $postdata, $headers);
		
		$response = json_decode($response, true);

		if ($response['e']) return false;
		
		return $response;
	}
	
	// ============================================================================
	public function addordermessage($idshippingmethod, $idorder, $postdata)
	{
		$url = $this->prepareurl($idshippingmethod, '/sale/wse_client_addordermessage');

		$headers = array();
		$headers[] = 'Authorization:'.$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['jwt'];

		$postdata['idorder'] = $idorder;
		$postdata['signature'] = $this->getsignature($idshippingmethod, $idorder);

		$response = $this->POST_REQUEST($url, $httpcode, $message, $postdata, $headers);
		
		$response = json_decode($response, true);

		if ($response['e']) return false;
		
		return $response;
	}
	
	// ============================================================================
	public function logoutall()
	{
		foreach ($this->SHIPPINGMETHODCONECTIONS as $k => $v)
		{
			$this->logout($k);
		}
	}
	
	// ============================================================================
	protected function logout($idshippingmethod)
	{
		$url = $this->prepareurl($idshippingmethod, '/user/wse_logout');

		$headers = array();
		$headers[] = 'Authorization:'.$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['jwt'];

		$postdata = array();

		$response = $this->POST_REQUEST($url, $httpcode, $message, $postdata, $headers);
		
		$response = json_decode($response, true);

		if ($response['e']) return false;
		
		return $response;
	}
	
	// ============================================================================
	public function iscorrectsignature($idshippingmethod, $signature, $prefix = '')
	{
		$newsignature = $this->getsignature($idshippingmethod, $prefix);
		if($newsignature != $signature) return false;
		return true;
	}
	
	// ============================================================================

	



	
	// ============================================================================
	public function getpaymethods($idshippingmethod, $country = NULL)
	{
		$url = $this->prepareurl($idshippingmethod, '/sale/wse_getpaymethods');

		$headers = array();
		$headers[] = 'Authorization:'.$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['jwt'];

		if (!$country) $country = 'ro';
		$signature = $this->getsignature($idshippingmethod, $country);

		$postdata = array (
			'signature' => $signature
			, 'country' => $country
		);

		$response = $this->POST_REQUEST($url, $httpcode, $message, $postdata, $headers);
		
		$response = json_decode($response, true);
	
		if ($response['e']) return false;
		
		return $response;
	}
	
	// ============================================================================
	public function getproducts($idshippingmethod, $country = NULL)
	{
		$url = $this->prepareurl($idshippingmethod, '/sale/wse_getproducts');

		$headers = array();
		$headers[] = 'Authorization:'.$this->SHIPPINGMETHODCONECTIONS[$idshippingmethod]['jwt'];

		if (!$country) $country = 'ro';
		$signature = $this->getsignature($idshippingmethod, $country);

		$postdata = array (
			'signature' => $signature
			, 'country' => $country
		);

		$response = $this->POST_REQUEST($url, $httpcode, $message, $postdata, $headers);
		
		$response = json_decode($response, true);
	
		if ($response['e']) return false;
		
		return $response['objects'];
	}
	
	// ============================================================================
}