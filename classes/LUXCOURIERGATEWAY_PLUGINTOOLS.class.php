<?php
class LUXCOURIERGATEWAY_PLUGINTOOLS
{
	// ============================================================================
	public static function createtablessql($prefix, $charset_collate)
	{
		$queries = array();

		$queries[] = 'CREATE TABLE IF NOT EXISTS `'.$prefix.'luxcouriergateway_notifyorder` (
			`id` bigint(20) NOT NULL AUTO_INCREMENT,
			`idorder` bigint(20) DEFAULT NULL,
			`tentatives` bigint(20) DEFAULT NULL,
			`nexttentative` bigint(20) DEFAULT NULL,
			PRIMARY KEY  (id),
			KEY `idorder` (`idorder`),
			KEY `tentatives` (`tentatives`),
			KEY `nexttentative` (`nexttentative`)
		  ) '.$charset_collate.';';

		$queries[] = 'CREATE TABLE IF NOT EXISTS `'.$prefix.'luxcouriergateway_orderinfo` (
			`id` bigint(20) NOT NULL AUTO_INCREMENT,
			`idorder` bigint(20) DEFAULT NULL,
			`idluxcourierorder` bigint(20) DEFAULT NULL,
			PRIMARY KEY  (id),
			KEY `idorder` (`idorder`),
			KEY `idluxcourierorder` (`idluxcourierorder`)
		  ) '.$charset_collate.';';

		$queries[] = 'CREATE TABLE IF NOT EXISTS `'.$prefix.'luxcouriergateway_comments` (
			`id` bigint(20) NOT NULL AUTO_INCREMENT,
			`idcomment` bigint(20) DEFAULT NULL,
			`tentatives` bigint(20) DEFAULT NULL,
			`nexttentative` bigint(20) DEFAULT NULL,
			PRIMARY KEY  (id),
			KEY `idcomment` (`idcomment`),
			KEY `tentatives` (`tentatives`),
			KEY `nexttentative` (`nexttentative`)
		  ) '.$charset_collate.';';

		$queries[] = 'CREATE TABLE IF NOT EXISTS `'.$prefix.'luxcouriergateway_orderinprocess` (
			`id` bigint(20) NOT NULL AUTO_INCREMENT,
			`idorder` bigint(20) DEFAULT NULL,
			`start` bigint(20) DEFAULT NULL,
			PRIMARY KEY  (id),
			KEY `idorder` (`idorder`),
			KEY `start` (`start`)
		) '.$charset_collate.';';

		$queries[] = 'CREATE TABLE IF NOT EXISTS `'.$prefix.'luxcouriergateway_logs` (
			`id` bigint(20) NOT NULL AUTO_INCREMENT,
			`data` bigint(20) DEFAULT NULL,
			`funct` varchar(300) DEFAULT NULL,
			`tip` varchar(50) DEFAULT NULL,
			`note` varchar(300) DEFAULT NULL,
			PRIMARY KEY  (id),
			KEY `data` (`data`)
		) '.$charset_collate.';';

		return $queries;
	}

	// ============================================================================
	public static function get_shippingtypes()
	{
		$rez = array();

		$rez[] = 'LUXSHIPPING';
		$rez[] = 'SIMPLESHIPPING';

		return $rez;
	}

	// ============================================================================
	public static function get_shippingmethodoptions($idshippingmethod)
	{
		$toptions = get_option('woocommerce_'.$idshippingmethod.'_settings');

		$arr = array();
		$arr['user'] = $toptions['user'];
		$arr['password'] = $toptions['password'];
		$arr['secretkey'] = $toptions['secretkey'];
		$arr['apiurl'] = $toptions['luxcourierapiurl'];

		$arr['client_name'] = $toptions['client_name'];
		$arr['client_company'] = $toptions['client_company'];
		$arr['client_site'] = $toptions['client_site'];
		$arr['client_address'] = $toptions['client_address'];
		$arr['client_phone'] = $toptions['client_phone'];
		$arr['client_contact'] = $toptions['client_contact'];

		return $arr;
	}

	// ============================================================================
	public static function getOrderDetailById($id, $fields = null, $filter = array()) {

        if (is_wp_error($id))
            return false;

        // Get the decimal precession
        $dp = (isset($filter['dp'])) ? intval($filter['dp']) : 2;
        $order = wc_get_order($id); //getting order Object

        if ($order === false)
            return false;

        $order_data = array(
            'id' => $order->get_id(),
            'order_number' => $order->get_order_number(),
            'created_at' => $order->get_date_created()->date('Y-m-d H:i:s'),
            'updated_at' => $order->get_date_modified()->date('Y-m-d H:i:s'),
            'completed_at' => !empty($order->get_date_completed()) ? $order->get_date_completed()->date('Y-m-d H:i:s') : '',
            'status' => $order->get_status(),
            'currency' => $order->get_currency(),
            'total' => wc_format_decimal($order->get_total(), $dp),
            'subtotal' => wc_format_decimal($order->get_subtotal(), $dp),
            'total_line_items_quantity' => $order->get_item_count(),
            'total_tax' => wc_format_decimal($order->get_total_tax(), $dp),
            'total_shipping' => wc_format_decimal($order->get_total_shipping(), $dp),
            'cart_tax' => wc_format_decimal($order->get_cart_tax(), $dp),
            'shipping_tax' => wc_format_decimal($order->get_shipping_tax(), $dp),
            'total_discount' => wc_format_decimal($order->get_total_discount(), $dp),
            'shipping_methods' => $order->get_shipping_method(),
            'order_key' => $order->get_order_key(),
            'payment_details' => array(
                'method_id' => $order->get_payment_method(),
                'method_title' => $order->get_payment_method_title(),
                'paid_at' => !empty($order->get_date_paid()) ? $order->get_date_paid()->date('Y-m-d H:i:s') : '',
            ),
            'billing_address' => array(
                'first_name' => $order->get_billing_first_name(),
                'last_name' => $order->get_billing_last_name(),
                'company' => $order->get_billing_company(),
                'address_1' => $order->get_billing_address_1(),
                'address_2' => $order->get_billing_address_2(),
                'city' => $order->get_billing_city(),
                'state' => $order->get_billing_state(),
                'formated_state' => 
					(isset(WC()->countries->states[$order->get_billing_country()][$order->get_billing_state()])) 
						? WC()->countries->states[$order->get_billing_country()][$order->get_billing_state()]
						: ''
					, //human readable formated state name
                'postcode' => $order->get_billing_postcode(),
                'country' => $order->get_billing_country(),
                'formated_country' => 
					($order->get_billing_country())
						? WC()->countries->countries[$order->get_billing_country()]
						: ''
						, //human readable formated country name
                'email' => $order->get_billing_email(),
                'phone' => $order->get_billing_phone()
            ),
            'shipping_address' => array(
                'first_name' => $order->get_shipping_first_name(),
                'last_name' => $order->get_shipping_last_name(),
                'company' => $order->get_shipping_company(),
                'address_1' => $order->get_shipping_address_1(),
                'address_2' => $order->get_shipping_address_2(),
                'city' => $order->get_shipping_city(),
                'state' => $order->get_shipping_state(),
                'formated_state' => 
					(isset(WC()->countries->states[$order->get_shipping_country()][$order->get_shipping_state()])) 
						? WC()->countries->states[$order->get_shipping_country()][$order->get_shipping_state()]
						: ''
					, //human readable formated state name
                // 'formated_state' => WC()->countries->states[$order->get_shipping_country()][$order->get_shipping_state()], //human readable formated state name
                'postcode' => $order->get_shipping_postcode(),
                'country' => $order->get_shipping_country(),
                'formated_country' => 
					($order->get_shipping_country()) 
						? WC()->countries->countries[$order->get_shipping_country()] 
						: ''
						//human readable formated country name
            ),
            'note' => $order->get_customer_note(),
            'customer_ip' => $order->get_customer_ip_address(),
            'customer_user_agent' => $order->get_customer_user_agent(),
            'customer_id' => $order->get_user_id(),
            'view_order_url' => $order->get_view_order_url(),
            'line_items' => array(),
            'shipping_lines' => array(),
            'tax_lines' => array(),
            'fee_lines' => array(),
            'coupon_lines' => array(),
        );

        //getting all line items
        foreach ($order->get_items() as $item_id => $item) {

            $product = $item->get_product();

            $product_id = null;
            $product_sku = null;
            // Check if the product exists.
            if (is_object($product)) {
                $product_id = $product->get_id();
                $product_sku = $product->get_sku();
            }

            $order_data['line_items'][] = array(
                'id' => $item_id,
                'subtotal' => wc_format_decimal($order->get_line_subtotal($item, false, false), $dp),
                'subtotal_tax' => wc_format_decimal($item['line_subtotal_tax'], $dp),
                'total' => wc_format_decimal($order->get_line_total($item, false, false), $dp),
                'total_tax' => wc_format_decimal($item['line_tax'], $dp),
                'price' => wc_format_decimal($order->get_item_total($item, false, false), $dp),
                'quantity' => wc_stock_amount($item['qty']),
                'tax_class' => (!empty($item['tax_class']) ) ? $item['tax_class'] : null,
                'name' => $item['name'],
                'product_id' => (!empty($item->get_variation_id()) && ('product_variation' === $product->post_type )) ? $product->get_parent_id() : $product_id,
                'variation_id' => (!empty($item->get_variation_id()) && ('product_variation' === $product->post_type )) ? $product_id : 0,
                'product_url' => get_permalink($product_id),
                'product_thumbnail_url' => wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'thumbnail', TRUE)[0],
                'sku' => $product_sku,
                'meta' => wc_display_item_meta($item, ['echo' => false])
            );
        }

        //getting shipping
        foreach ($order->get_shipping_methods() as $shipping_item_id => $shipping_item) {
            $order_data['shipping_lines'][] = array(
                'id' => $shipping_item_id,
                'method_id' => $shipping_item['method_id'],
                'method_title' => $shipping_item['name'],
                'total' => wc_format_decimal($shipping_item['cost'], $dp),
            );
        }

        //getting taxes
        foreach ($order->get_tax_totals() as $tax_code => $tax) {
            $order_data['tax_lines'][] = array(
                'id' => $tax->id,
                'rate_id' => $tax->rate_id,
                'code' => $tax_code,
                'title' => $tax->label,
                'total' => wc_format_decimal($tax->amount, $dp),
                'compound' => (bool) $tax->is_compound,
            );
        }

        //getting fees
        foreach ($order->get_fees() as $fee_item_id => $fee_item) {
            $order_data['fee_lines'][] = array(
                'id' => $fee_item_id,
                'title' => $fee_item['name'],
                'tax_class' => (!empty($fee_item['tax_class']) ) ? $fee_item['tax_class'] : null,
                'total' => wc_format_decimal($order->get_line_total($fee_item), $dp),
                'total_tax' => wc_format_decimal($order->get_line_tax($fee_item), $dp),
            );
        }

        //getting coupons
        foreach ($order->get_items('coupon') as $coupon_item_id => $coupon_item) {

            $order_data['coupon_lines'][] = array(
                'id' => $coupon_item_id,
                'code' => $coupon_item['name'],
                'amount' => wc_format_decimal($coupon_item['discount_amount'], $dp),
            );
        }
        
        
		// -------------------------------------
        $field_delivery_date = 'delivery_date';
        $field_delivery_time = 'delivery_time';
		// -------------------------------------

        $method_id = luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order_data);

        if (strpos($method_id,'LUXCOURIERGATEWAY_') !== false) 
        {
            $toptions = get_option('woocommerce_'.$method_id.'_settings');
            if (is_array($toptions))
            {
                if (isset($toptions['field_delivery_date']) && $toptions['field_delivery_date']) $field_delivery_date = $toptions['field_delivery_date'];
                if (isset($toptions['field_delivery_time']) && $toptions['field_delivery_time']) $field_delivery_time = $toptions['field_delivery_time'];
            }
        }

		// -------------------------------------
		$delivery_date = '';
		$delivery_interval = '';
		$delivery_datefull = '';
		$delivery_time = '';

		$allmeta = (array)$order->get_meta_data();
		foreach ($allmeta as $v)
		{
			$k = $v->get_data();

			if ($k['key'] == $field_delivery_date) $delivery_date = $k['value'];
			if ($k['key'] == $field_delivery_time) $delivery_interval = $k['value'];
		}

		if ($delivery_date && $delivery_interval)
		{
            if (( is_numeric($delivery_date) && (int)$delivery_date == $delivery_date ))
            {
                // timestamp
                $t = explode(',', $delivery_interval);
                $ts = (int)reset($t);
                $te = (int)end($t);
                $ts = $ts * 60;
                $te = $te * 60;

                $sh = floor($ts/3600);
                $sm = ($ts - ($sh * 3600)) / 60;

                $eh = floor($te/3600);
                $em = ($te - ($eh * 3600)) / 60;

                $delivery_interval = 
                    str_pad($sh, 2, "0", STR_PAD_LEFT)
                    .':'
                    .str_pad($sm, 2, "0", STR_PAD_LEFT)
                    .'-'
                    .str_pad($eh, 2, "0", STR_PAD_LEFT)
                    .':'
                    .str_pad($em, 2, "0", STR_PAD_LEFT)
                ;


                $delivery_time = $delivery_date + $ts;
                $delivery_datefull = date('Y-m-d H:i', $delivery_time);
                $delivery_date = date('Y-m-d', $delivery_time);
            } else
            {
                $t = explode('-', $delivery_interval);
                $delivery_datefull = $delivery_date.' '.trim(reset($t));
                $delivery_time = strtotime($delivery_datefull);
            }
		}

		
        // delivery data
		$order_data['delivery_data'] = array(
			'delivery_date' => $delivery_date,
			'delivery_interval' => $delivery_interval,
			'delivery_datefull' => $delivery_datefull,
			'delivery_time' => $delivery_time,
		);
		// -------------------------------------

        return apply_filters('woocommerce_api_order_response', $order_data, $order, $fields);
    }

	// ============================================================================
}