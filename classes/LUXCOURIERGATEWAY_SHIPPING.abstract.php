<?php
class LUXCOURIERGATEWAY_SHIPPING extends WC_Shipping_Method{

	public function __construct($instance_id, $suffixid){
		  $this->id = 'LUXCOURIERGATEWAY_'.$suffixid;
			
			$this->method_title = __( $this->id, 'luxcouriergateway' );
			$this->method_description  = __( $this->id.' Description', 'luxcouriergateway' );

			$this->instance_id = absint( $instance_id );

			$this->supports  = array(
			   'shipping-zones',
				'instance-settings',
				'instance-settings-modal',
			 );

			// Load the settings.
			$this->init_form_fields();
			$this->init_instance_form_fields();
			// $this->init_settings_fields();
			$this->init_settings();
			$this->init_instance_settings();

			// Define user set variables
			$this->title = $this->get_option( 'title' ) ? $this->get_option( 'title' ) : __( $this->id.' Name', 'luxcouriergateway' );

			$this->enabled			= $this->get_option( 'enabled' );
			$this->showcheckouticon			= $this->get_option( 'showcheckouticon' );
			$this->user 			= $this->get_option( 'user' );
			$this->password			= $this->get_option( 'password' );
			$this->secretkey		= $this->get_option( 'secretkey' );
			$this->luxcourierapiurl	= $this->get_option( 'luxcourierapiurl' );

			$this->client_name		= $this->get_option( 'client_name' );
			$this->client_company	= $this->get_option( 'client_company' );
			$this->client_site		= $this->get_option( 'client_site' );
			$this->client_address	= $this->get_option( 'client_address' );
			$this->client_phone		= $this->get_option( 'client_phone' );
			$this->client_contact	= $this->get_option( 'client_contact' );

			$this->start_address	= $this->get_option( 'start_address' );

			// $this->use_fixedprice	= $this->get_option( 'use_fixedprice' );
			$this->fixedpricevalue	= $this->get_option( 'fixedpricevalue' );
			// $this->dinamiquepricestartvalue	= $this->get_option( 'dinamiquepricestartvalue' );
			// $this->dinamiquepriceperkmvalue	= $this->get_option( 'dinamiquepriceperkmvalue' );

			add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
	}

	public function init_form_fields(){
			$this->form_fields = array(
			  'enabled' => array(
				'title'                 => __( 'Enable/Disable', 'woocommerce' ),
				'type'                         => 'checkbox',
				'label'                 => __( 'Enable '.$this->id, 'luxcouriergateway' ),
				'default'                 => 'yes'
			  )
			  , 'showcheckouticon' => array(
				'title'                 => __( 'showcheckouticon', 'luxcouriergateway' ),
				'type'                         => 'checkbox',
				'label'                 => __( 'showcheckouticon description', 'luxcouriergateway' ),
				'default'                 => 'yes'
			  )
			  , 'user' => array(
				'title'                 => __( 'user', 'luxcouriergateway' ),
				'type'                         => 'text',
				'description'         => __( 'This controls the user from luxcourier system.', 'luxcouriergateway' ),
				'default'                => ''
			  )
			  , 'password' => array(
				'title'                 => __( 'password', 'luxcouriergateway' ),
				'type'                         => 'password',
				'description'         => __( 'This controls the password from luxcourier system.', 'luxcouriergateway' ),
				'default'                => ''
			  )
			  , 'secretkey' => array(
				'title'                 => __( 'secretkey', 'luxcouriergateway' ),
				'type'                         => 'text',
				'description'         => __( 'This controls the secretkey from luxcourier system.', 'luxcouriergateway' ),
				'default'                => ''
			  )
			  , 'luxcourierapiurl' => array(
				'title'                 => __( 'luxcourierapiurl', 'luxcouriergateway' ),
				'type'                         => 'text',
				'description'         => __( 'This controls the API address of luxcourier system.', 'luxcouriergateway' ),
				'default'                => ''
			  )
			  , 'field_delivery_date' => array(
				'title'                 => __( 'field_delivery_date', 'luxcouriergateway' ),
				'type'                         => 'text',
				'description'         => __( 'This controls the field_delivery_date from woocommerce checkout.', 'luxcouriergateway' ),
				'default'                => 'delivery_date' 
			  )
			  , 'field_delivery_time' => array(
				'title'                 => __( 'field_delivery_time', 'luxcouriergateway' ),
				'type'                         => 'text',
				'description'         => __( 'This controls the field_delivery_time from woocommerce checkout.', 'luxcouriergateway' ),
				'default'                => 'delivery_time' 
			  )
			  , 'client_name' => array(
				'title'                 => __( 'client_name', 'luxcouriergateway' ),
				'type'                         => 'text',
				'description'         => __( 'This controls the client_name for luxcourier system.', 'luxcouriergateway' ),
				'default'                => '' 
			  )
			  , 'client_company' => array(
				'title'                 => __( 'client_company', 'luxcouriergateway' ),
				'type'                         => 'text',
				'description'         => __( 'This controls the client_company for luxcourier system.', 'luxcouriergateway' ),
				'default'                => '' 
			  )
			  , 'client_site' => array(
				'title'                 => __( 'client_site', 'luxcouriergateway' ),
				'type'                         => 'text',
				'description'         => __( 'This controls the client_site for luxcourier system.', 'luxcouriergateway' ),
				'default'                => '' 
			  )
			  , 'client_address' => array(
				'title'                 => __( 'client_address', 'luxcouriergateway' ),
				'type'                         => 'text',
				'description'         => __( 'This controls the client_address for luxcourier system.', 'luxcouriergateway' ),
				'default'                => '' 
			  )
			  , 'client_phone' => array(
				'title'                 => __( 'client_phone', 'luxcouriergateway' ),
				'type'                         => 'text',
				'description'         => __( 'This controls the client_phone for luxcourier system.', 'luxcouriergateway' ),
				'default'                => '' 
			  )
			  , 'client_contact' => array(
				'title'                 => __( 'client_contact', 'luxcouriergateway' ),
				'type'                         => 'text',
				'description'         => __( 'This controls the client_contact for luxcourier system.', 'luxcouriergateway' ),
				'default'                => '' 
			  )
			  , 'start_address' => array(
				'title'                 => __( 'start_address', 'luxcouriergateway' ),
				'type'                         => 'text',
				'description'         => __( 'This controls the start_address', 'luxcouriergateway' ),
				'default'                => '' 
			  )
		  );
	}

	public function init_instance_form_fields(){
			$this->instance_form_fields = array(
			  'title' => array(
				'title'                 => __( 'title', 'luxcouriergateway' ),
				'type'                  => 'text',
				'desc_tip'         		=> __( 'This controls the method name visible for user', 'luxcouriergateway' ),
				'default'               => __( $this->id.' Name', 'luxcouriergateway' ) 
			  )
			//   , 'use_fixedprice' => array(
			// 	'title'                 => __( 'use_fixedprice', 'luxcouriergateway' ),
			// 	'type'                         => 'checkbox',
			// 	'label'                 => __( 'use_fixedprice for shipping', 'luxcouriergateway' ),
			// 	'desc_tip'         		=> __( 'use_fixedprice description', 'luxcouriergateway' ),
			// 	'default'                 => 'yes'
			//   )
			  , 'fixedpricevalue' => array(
				'title'                 => __( 'fixedpricevalue', 'luxcouriergateway' ),
				'type'                  => 'text',
				'desc_tip'         		=> __( 'fixedpricevalue description', 'luxcouriergateway' ),
				'default'               => '0' 
			  )
			//   , 'dinamiquepricestartvalue' => array(
			// 	'title'                 => __( 'dinamiquepricestartvalue', 'luxcouriergateway' ),
			// 	'type'                  => 'text',
			// 	'desc_tip'         		=> __( 'dinamiquepricestartvalue description', 'luxcouriergateway' ),
			// 	'default'               => '0' 
			//   )
			//   , 'dinamiquepriceperkmvalue' => array(
			// 	'title'                 => __( 'dinamiquepriceperkmvalue', 'luxcouriergateway' ),
			// 	'type'                  => 'text',
			// 	'desc_tip'         		=> __( 'dinamiquepriceperkmvalue description', 'luxcouriergateway' ),
			// 	'default'               => '0' 
			//   )
		  );
	}

	public function get_instance_form_fields() {
		return apply_filters( 'woocommerce_shipping_instance_form_fields_' . $this->id, array_map( array( $this, 'set_defaults' ), $this->instance_form_fields ) );
	}

	public function supports( $feature ){
		// echo '<pre>';
		// print_r($feature);
		// echo '</pre>';
		// exit();
			return true;
	}
	public function is_available( $package ){

		// echo '<pre>';
		// print_r($package);
		// echo '</pre>';
		// exit();

		// 	foreach ( $package['contents'] as $item_id => $values ) {
		// $_product = $values['data'];
		// $weight =        $_product->get_weight();
		// if($weight > 10){
		// 		return false;
		// }
		// 	}
			return true;
	}
	public function calculate_shipping($package = []){

		$c= $this->add_rate( array(
			'id'         => $this->id,
			'label' => $this->title,
			'cost'         => $this->fixedpricevalue
		  ));

		  return;
	}
}