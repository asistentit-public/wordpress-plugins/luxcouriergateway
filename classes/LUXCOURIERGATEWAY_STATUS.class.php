<?php
class LUXCOURIERGATEWAY_STATUS
{
  const ORD_UNFINISHED    = 200;
  const ORD_PENDING       = 201;
  const ORD_ACCPTCOURIER  = 202;
  const ORD_READYFORTAKE  = 203;
  const ORD_TAKED         = 204;
  const ORD_STARTDELIVERED= 206;
  const ORD_DELIVERED     = 205;

  const ORD_CANCELED      = 210;
  const ORD_ARCHIVE       = 211;
  const ORD_RETURNED      = 212;

  const ORD_PAY_UNPAID    = 220;
  const ORD_PAY_PAID      = 221;
  const ORD_PAY_CANCELED  = 222;
  const ORD_PAY_RETURNED  = 223;

}