<?php
class LUXCOURIERGATEWAY_TYPEDELIVERY 
{
	const LUXSHIPPING			= 10;	// 
	const SIMPLESHIPPING	= 20;	// 

  public static function GET_TYPEDELIVERY($idshipmethod)
  {
    $rez = 10;
    
    if ($idshipmethod == 'LUXCOURIERGATEWAY_LUXSHIPPING') $rez = 10;
    if ($idshipmethod == 'LUXCOURIERGATEWAY_SIMPLESHIPPING') $rez = 20;

    return $rez;
  }

}