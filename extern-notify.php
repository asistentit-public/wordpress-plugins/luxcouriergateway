<?php
	ini_set('display_errors', 1);
    define('WP_USE_THEMES', true);
    
    define('SITE_DIR', str_replace("wp-content".DIRECTORY_SEPARATOR."plugins".DIRECTORY_SEPARATOR.basename(dirname(__FILE__)),"",__DIR__));
    require_once SITE_DIR . DIRECTORY_SEPARATOR . 'wp-load.php';
    wp();

    require_once (LUXCOURIERGATEWAY_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'LUXCOURIERGATEWAY_STATUS.class.php');
    require_once (LUXCOURIERGATEWAY_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'LUXCOURIERGATEWAY_ADDRESSTYPE.class.php');
    require_once (LUXCOURIERGATEWAY_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'LUXCOURIERGATEWAY_TYPEDELIVERY.class.php');
    require_once (LUXCOURIERGATEWAY_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'LUXCOURIERGATEWAY_PLUGINTOOLS.class.php');
    require_once (LUXCOURIERGATEWAY_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'LUXCOURIERGATEWAYTOOLS.class.php');
    
	// ============================================================================

	// ============================================================================
    function luxcouriergateway_externnotification_checkmandatorydata()
    {
		if (!isset($_POST['field']) || !$_POST['field'])
		{
			echo 'ok';
			exit();
		}
		if (!isset($_POST['signature']) || !$_POST['signature'])
		{
			echo 'ok';
			exit();
		}
		if (!isset($_POST['idorder']) || !$_POST['idorder'])
		{
			echo 'ok';
			exit();
		}
		if (!(int)$_GET['idorder'])
		{
			echo 'ok';
			exit();
		}

		if (!in_array($_POST['field'], array(
			'status'			
			, 'paystatus'
			, 'end_time_real'

			, 'idcourier'
			, 'courier_text'
			, 'courier_phone'

			, 'message'
			, 'photo'

			, 'checkforaccept'
		)))
		{
			echo 'ok';
			exit();			
		}
        
    }
	// ============================================================================
	function luxcouriergateway_externnotification()
	{
        $idorder = $_GET['idorder'];
        $orderarr = LUXCOURIERGATEWAY_PLUGINTOOLS::getOrderDetailById($idorder);

        $orderinfo = luxcouriergateway_cron_minute_get_orderinfo_obj($idorder);

		// -----------------------------------------------------
        if (!$orderinfo || !$orderinfo->id)
		{
			echo 'ok';
			exit();
		}

		if ($orderinfo->idluxcourierorder != $_POST['idorder'])
		{
			echo 'ok';
			exit();
		}
		// -----------------------------------------------------

		// -----------------------------------------------------
        $LUXCOURIERGATEWAYTOOLSOBJ = new LUXCOURIERGATEWAYTOOLS();
		$t = $LUXCOURIERGATEWAYTOOLSOBJ->checkcompanydetail(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($orderarr));
		if (!$t) 
		{
			echo 'ok';
			exit();
		}
		// -----------------------------------------------------

		// -----------------------------------------------------
		if (!$LUXCOURIERGATEWAYTOOLSOBJ->iscorrectsignature(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($orderarr), $_POST['signature'], $_POST['idorder']))
		{
			echo 'err';
			exit();
		}
		// -----------------------------------------------------

		// -----------------------------------------------------
        $order = new WC_Order($idorder);
		// -----------------------------------------------------


		// -----------------------------------------------------
		if ($_POST['field'] == 'status')
		{
			if ($_POST['newvalue'] == LUXCOURIERGATEWAY_STATUS::ORD_TAKED) 
            {
                // $order->update_status('on-hold', 'LUXCOURIER'); 
            }
			if ($_POST['newvalue'] == LUXCOURIERGATEWAY_STATUS::ORD_STARTDELIVERED) 
			{
                // $order->update_status('on-hold', 'LUXCOURIER'); 
			}
			if ($_POST['newvalue'] == LUXCOURIERGATEWAY_STATUS::ORD_DELIVERED) 
			{
                $order->update_status('completed', 'LUXCOURIER'); 
                $order->add_order_note( 'LUXCOURIER -> Status: completed' );
			}
			if ($_POST['newvalue'] == LUXCOURIERGATEWAY_STATUS::ORD_ARCHIVE) 
			{
                $order->update_status('completed', 'LUXCOURIER');
                $order->add_order_note( 'LUXCOURIER -> Status: completed' ); 
			}
			if ($_POST['newvalue'] == LUXCOURIERGATEWAY_STATUS::ORD_CANCELED) 
			{
                $order->update_status('pending', 'LUXCOURIER'); 
                $order->add_order_note( 'LUXCOURIER -> Status: pending (cancel)' );
			}
			if ($_POST['newvalue'] == LUXCOURIERGATEWAY_STATUS::ORD_RETURNED) 
			{
                $order->update_status('pending', 'LUXCOURIER'); 
                $order->add_order_note( 'LUXCOURIER -> Status: pending (returned)' );
			}
			echo 'ok';
			exit();
		}
		// -----------------------------------------------------
        
		// -----------------------------------------------------
		if ($_POST['field'] == 'paystatus')
		{
			if (
					$_POST['newvalue'] == LUXCOURIERGATEWAY_STATUS::ORD_PAY_PAID
					&& $order->get_payment_method() == 'cod'
				) 
			{
                $order->add_order_note( 'LUXCOURIER -> order is payed on courier' );
				
				echo 'ok';
				exit();
			}
		}
		// -----------------------------------------------------
        
		// -----------------------------------------------------
		if ($_POST['field'] == 'end_time_real')
		{
            $order->add_order_note( 'LUXCOURIER -> delivered date: '.date('Y-m-d H:i', $_POST['newvalue']) );
			echo 'ok';
			exit();
		}
		// -----------------------------------------------------

		// -----------------------------------------------------
		if ($_POST['field'] == 'courier_text')
		{
            $order->add_order_note( 'LUXCOURIER -> courier: '.$_POST['newvalue'] );
			echo 'ok';
			exit();
		}
		// -----------------------------------------------------

		// -----------------------------------------------------
		if ($_POST['field'] == 'courier_phone')
		{
            $order->add_order_note( 'LUXCOURIER -> courier phone: '.$_POST['newvalue'] );
			echo 'ok';
			exit();
		}
		// -----------------------------------------------------

		// -----------------------------------------------------
		if ($_POST['field'] == 'idcourier')
		{
			echo 'ok';
			exit();
		}
		// -----------------------------------------------------

		// -----------------------------------------------------
		if ($_POST['field'] == 'checkforaccept')
		{
			echo 'ok';
			exit();
		}
		// -----------------------------------------------------

		// -----------------------------------------------------
		if ($_POST['field'] == 'message')
		{
            $order->add_order_note( 'LUXCOURIER -> '.$_POST['newvalue'] );
			echo 'ok';
			exit();
		}
		// -----------------------------------------------------

		// -----------------------------------------------------
		if ($_POST['field'] == 'photo')
		{
            $order->add_order_note( 'LUXCOURIER -> New photo: '.'<a href="'.$_POST['newvalue'].'" target="_blank">'.$_POST['newvalue'].'</a>' );
			echo 'ok';
			exit();
		}
		// -----------------------------------------------------

        echo 'ok';
        exit();		
	}	
	
	// ============================================================================

	// ============================================================================
    
    luxcouriergateway_externnotification_checkmandatorydata();
    luxcouriergateway_externnotification();

    exit();




    