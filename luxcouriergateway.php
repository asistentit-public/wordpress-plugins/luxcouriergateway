<?php
/*
 * Plugin Name: luxcouriergateway
 * Plugin URI: https://gitlab.com/asistentit-public/wordpress-plugins/luxcouriergateway
 * Description: A wordpress plugin designed to integrate site orders with the LuxCourier system 
 * Version: 1.33 
 * Author: Asistent-IT
 * Author URI: https://asistentit.md
 * Text Domain: luxcouriergateway
 * Domain Path: /languages/
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/


/*  Copyright 2021  LuxCourierITTeem  (email: admin@luxcourier.app)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// ==============================================
// define('LUXCOURIERGATEWAY_DIR', plugin_dir_path(__FILE__));
define('LUXCOURIERGATEWAY_DIR', dirname(__FILE__));
define('LUXCOURIERGATEWAY_MAINDIR', basename(dirname(__FILE__)));

// ==============================================


require_once (LUXCOURIERGATEWAY_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'LUXCOURIERGATEWAY_STATUS.class.php');
require_once (LUXCOURIERGATEWAY_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'LUXCOURIERGATEWAY_ADDRESSTYPE.class.php');
require_once (LUXCOURIERGATEWAY_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'LUXCOURIERGATEWAY_TYPEDELIVERY.class.php');
require_once (LUXCOURIERGATEWAY_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'LUXCOURIERGATEWAY_PLUGINTOOLS.class.php');
require_once (LUXCOURIERGATEWAY_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'LUXCOURIERGATEWAYTOOLS.class.php');

// ==============================================

$load_text= load_plugin_textdomain( 'luxcouriergateway', '/wp-content/plugins/'. LUXCOURIERGATEWAY_MAINDIR . '/languages' , LUXCOURIERGATEWAY_MAINDIR  . '/languages' );

// ==============================================
function luxcouriergateway_activate() 
{
    //error_log('1');
    // ---------------------------------
    global $wpdb;
    
    $queries = LUXCOURIERGATEWAY_PLUGINTOOLS::createtablessql($wpdb->prefix, $wpdb->get_charset_collate());

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    
    foreach($queries as $sql)
    {
        dbDelta( $sql );
    }

    // ---------------------------------
    // luxcouriergateway_setrewriterules();
    // global $wp_rewrite;
    // $wp_rewrite->flush_rules(true);  
    // ---------------------------------
}

// ==============================================
function luxcouriergateway_deactivate() 
{
    //error_log('2');
    // global $wp_rewrite;
    // $wp_rewrite->flush_rules(true);
}

// ==============================================
// function luxcouriergateway_setrewriterules() 
// {
//     add_rewrite_rule( '(luxcouriergateway-notify)/$', 'wp-content/plugins/'.LUXCOURIERGATEWAY_MAINDIR.'/extern-notify.php', 'top' );
// }

// ==============================================
function luxcouriergateway_add_shipping_method_simpleshipping( $methods ) 
{
    //error_log('3');
    $all = LUXCOURIERGATEWAY_PLUGINTOOLS::get_shippingtypes();
    foreach ($all as $v)
    {
        $methods['LUXCOURIERGATEWAY_'.$v] = 'LUXCOURIERGATEWAY_'.$v;
    }
    return $methods;
}

// ==============================================
function luxcouriergateway_init_shipping_method_simpleshipping()
{
    //error_log('4');
    require_once (LUXCOURIERGATEWAY_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'LUXCOURIERGATEWAY_SHIPPING.abstract.php');

    $all = LUXCOURIERGATEWAY_PLUGINTOOLS::get_shippingtypes();
    foreach ($all as $v)
    {
        require_once (LUXCOURIERGATEWAY_DIR.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'LUXCOURIERGATEWAY_'.$v.'.class.php');
    }
}

// ==============================================
function luxcouriergateway_cart_shipping_method_full_label( $label, $method ) 
{
    //error_log('5');
    luxcouriergateway_init_shipping_method_simpleshipping();
    
    $all = LUXCOURIERGATEWAY_PLUGINTOOLS::get_shippingtypes();
    foreach ($all as $v)
    {
        if( $method->method_id === "LUXCOURIERGATEWAY_".$v ) 
        {
            $clname = "LUXCOURIERGATEWAY_".$v;
            $tobj = new $clname($method->instance_id);
    
            if ($tobj->settings['showcheckouticon'] == 'yes')
            {
                // $label = "<img src='".plugin_dir_url('luxcouriergateway').LUXCOURIERGATEWAY_MAINDIR."/resourse/icon/".$v.".png' style='height: 100px; border-radius: 5px; margin-left: 5px; margin-right: 5px;' />" . $label;

                $newlabel = '<div style="overflow:auto">';
                $newlabel .= '<div style="overflow:auto">';
                $newlabel .= $label;
                $newlabel .='</div>';
                $newlabel .= '<div style="overflow:auto">';
                $newlabel .= "<img src='".plugin_dir_url('luxcouriergateway').LUXCOURIERGATEWAY_MAINDIR."/resourse/icon/".$v.".png' style='height: 100px; border-radius: 5px; margin-left: 5px; margin-right: 5px;' />";
                $newlabel .='</div>';
                $newlabel .='</div>';


                $label = $newlabel;
            }
            break;
        } 
    }
    
    return $label; 
}

// ==============================================
function luxcouriergateway_checkout_order_created( $order )
{
    //error_log('6');
    $idorder = $order->get_id();

    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder );
    
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_notifyorder', array( 'idorder' => $idorder, 'tentatives' => 0, 'nexttentative' => time() ) );

    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder );
}

// ==============================================
function luxcouriergateway_admin_order_actions_end( $order )
{
    //error_log('11-111');
    $idorder = $order->get_id();

    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder );
    
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_notifyorder', array( 'idorder' => $idorder, 'tentatives' => 0, 'nexttentative' => time() ) );

    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder );
}

// ==============================================
function luxcouriergateway_process_shop_order_meta( $idorder )
{

    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder );
    //error_log('7');
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_notifyorder', array( 'idorder' => $idorder, 'tentatives' => 0, 'nexttentative' => time() ) );

    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder );
}

// ==============================================
function luxcouriergateway_update_order( $idorder )
{

    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder );
    //error_log('11-112');
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_notifyorder', array( 'idorder' => $idorder, 'tentatives' => 0, 'nexttentative' => time() ) );

    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder );
}

// ==============================================
function luxcouriergateway_order_payment_status_changed( $idorder )
{

    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder );
    //error_log('7');
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_notifyorder', array( 'idorder' => $idorder, 'tentatives' => 0, 'nexttentative' => time() ) );

    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder );
}

// ==============================================
function luxcouriergateway_view_order( $idorder )
{

    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder );
    //error_log('11-114');
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_notifyorder', array( 'idorder' => $idorder, 'tentatives' => 0, 'nexttentative' => time() ) );

    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder );
}

// ==============================================
function luxcouriergateway_new_order( $idorder )
{

    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder );
    //error_log('11-114');
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_notifyorder', array( 'idorder' => $idorder, 'tentatives' => 0, 'nexttentative' => time() ) );

    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder );
}

// ==============================================
function luxcouriergateway_shop_order( $idorder )
{

    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder );
    //error_log('11-114');
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_notifyorder', array( 'idorder' => $idorder, 'tentatives' => 0, 'nexttentative' => time() ) );

    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder );
}

// ==============================================
function luxcouriergateway_comment_post( $idcomment )
{

    luxcouriergateway_savelog( __FUNCTION__, 's', 'idcomment -> '.$idcomment );
    //error_log('8');
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_comments', array( 'idcomment' => $idcomment, 'tentatives' => 0, 'nexttentative' => time() ) );

    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idcomment -> '.$idcomment );
}

// ==============================================
function luxcouriergateway_order_note_added( $idcomment )
{

    luxcouriergateway_savelog( __FUNCTION__, 's', 'idcomment -> '.$idcomment );
    //error_log('8');
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_comments', array( 'idcomment' => $idcomment, 'tentatives' => 0, 'nexttentative' => time() ) );

    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idcomment -> '.$idcomment );
}

// ==============================================
function luxcouriergateway_note_created( $idcomment )
{
    luxcouriergateway_savelog( __FUNCTION__, 's', 'idcomment -> '.$idcomment );
    //error_log('8');
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_comments', array( 'idcomment' => $idcomment, 'tentatives' => 0, 'nexttentative' => time() ) );
    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idcomment -> '.$idcomment );
}

// ==============================================
$active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );

if ( 
        in_array( 'woocommerce/woocommerce.php', $active_plugins) 
        // && (
        //     in_array( 'woo-delivery/coderockz-woo-delivery.php', $active_plugins) 
        //     || in_array( 'woo-checkout-field-editor-pro/checkout-form-designer.php', $active_plugins) 
        // )
    ) 
{
    add_action( 'woocommerce_shipping_init', 'luxcouriergateway_init_shipping_method_simpleshipping' );
    add_filter( 'woocommerce_shipping_methods', 'luxcouriergateway_add_shipping_method_simpleshipping' );
    add_filter( 'woocommerce_cart_shipping_method_full_label', 'luxcouriergateway_cart_shipping_method_full_label', 10, 2 ); 
    
    add_action( 'save_post_shop_order', 'luxcouriergateway_shop_order' );
    add_action( 'comment_post', 'luxcouriergateway_comment_post' );

    // add_action( 'woocommerce_new_order', 'luxcouriergateway_new_order' );

    // add_action( 'woocommerce_checkout_order_created', 'luxcouriergateway_checkout_order_created' );
    // add_action( 'woocommerce_process_shop_order_meta', 'luxcouriergateway_process_shop_order_meta' );
    // add_action( 'woocommerce_order_payment_status_changed', 'luxcouriergateway_order_payment_status_changed' );
    // add_action( 'woocommerce_admin_order_actions_end', 'luxcouriergateway_admin_order_actions_end' );
    // add_action( 'woocommerce_update_order', 'luxcouriergateway_update_order' );
    // add_action( 'woocommerce_view_order', 'luxcouriergateway_view_order' );

    // add_action( 'woocommerce_order_note_added', 'luxcouriergateway_order_note_added' );
    // add_action( 'woocommerce_note_created', 'luxcouriergateway_note_created' );
}

// ==============================================
register_activation_hook( __FILE__, 'luxcouriergateway_activate' );
register_deactivation_hook(__FILE__,'luxcouriergateway_deactivate');
// add_action('init', 'luxcouriergateway_setrewriterules');
// add_filter( 'auto_update_plugin', '__return_true' );

// ==============================================

// ==============================================
// ==============================================
// ==============================================
// ==============================================

// ==============================================
add_filter( 'cron_schedules', 'luxcouriergateway_cron_add_every_minute' );
add_action( 'wp', 'luxcouriergateway_activate_cron_every_minute' );
add_action( 'luxcouriergateway_every_minute_event', 'luxcouriergateway_cron_every_minute_event' );

// ==============================================
function luxcouriergateway_cron_add_every_minute( $schedules ) 
{
    // luxcouriergateway_savelog( __FUNCTION__, 's' );
    // luxcouriergateway_cron_every_minute_event_action();
    //error_log('9');
	$schedules['luxcouriergateway_every_minute'] = array(
		'interval' => 60,
		'display' => 'every minute'
	);
    // luxcouriergateway_savelog( __FUNCTION__, 'e' );
	return $schedules;
}

// ==============================================
function luxcouriergateway_activate_cron_every_minute() 
{
    // luxcouriergateway_savelog( __FUNCTION__, 's' );
    //error_log('10');
	if ( ! wp_next_scheduled( 'luxcouriergateway_every_minute_event' ) )
		wp_schedule_event( time(), 'luxcouriergateway_every_minute', 'luxcouriergateway_every_minute_event' );
    // luxcouriergateway_savelog( __FUNCTION__, 'e' );
}

// ==============================================
function luxcouriergateway_cron_every_minute_event() 
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    //error_log('luxcouriergateway_cron_every_minute_event');
    luxcouriergateway_cron_every_minute_event_action();
    luxcouriergateway_savelog( __FUNCTION__, 'e' );
}

// ==============================================
function luxcouriergateway_cron_every_minute_event_action() 
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    //error_log('luxcouriergateway_cron_every_minute_event_action');
    luxcouriergateway_cron_minute_process_order_notify();
    luxcouriergateway_cron_minute_clear_orderinprocess();
    luxcouriergateway_cron_minute_clear_orderlogs();
    luxcouriergateway_cron_minute_process_order_comment();
    luxcouriergateway_savelog( __FUNCTION__, 'e' );
}

// ==============================================

// ==============================================
// ==============================================
// ==============================================
// ==============================================



// ==============================================
function luxcouriergateway_cron_minute_process_order_comment()
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    global $wpdb;
    $all = $wpdb->get_results( " SELECT * FROM ".$wpdb->prefix."luxcouriergateway_comments WHERE `nexttentative` <= ".time()." ORDER BY `nexttentative`, `id` LIMIT 2 ; " );

    $LUXCOURIERGATEWAYTOOLSOBJ = new LUXCOURIERGATEWAYTOOLS();
    
    foreach ($all as $v)
    {
        $allcomments = $wpdb->get_results( " SELECT * FROM ".$wpdb->prefix."comments WHERE `comment_ID` = ".$v->idcomment."  ; " );
        $comment = reset($allcomments);

        if (strtolower($comment->comment_agent) != 'woocommerce')
        {
            luxcouriergateway_cron_minute_process_order_comment_setprocesedobj($v);
            continue;
        }
        
        $order = LUXCOURIERGATEWAY_PLUGINTOOLS::getOrderDetailById($comment->comment_post_ID);
        $orderinfo = luxcouriergateway_cron_minute_get_orderinfo_obj($order['id']);

        if (!$orderinfo)
        {
            luxcouriergateway_cron_minute_process_order_comment_setprocesedobj($v);
            continue;
        }

        $LUXCOURIERGATEWAYTOOLSOBJ->checkcompanydetail(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order));

        $postdata = array();
        $postdata['message'] = $comment->comment_content;
        $postdata['messagetime'] = strtotime($comment->comment_date);
        $LUXCOURIERGATEWAYTOOLSOBJ->addordermessage(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order), $order['id'], $postdata);
        
        luxcouriergateway_cron_minute_process_order_comment_setprocesedobj($v);
    }

    $LUXCOURIERGATEWAYTOOLSOBJ->logoutall();
    luxcouriergateway_savelog( __FUNCTION__, 'e' );
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_comment_setprocesedobj($notifycomment)
{
    luxcouriergateway_savelog( __FUNCTION__, 's', 'idcomment -> '.$notifycomment->id );
    global $wpdb;
    $wpdb->delete( $wpdb->prefix.'luxcouriergateway_comments', array( 'id' => $notifycomment->id ) );
    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idcomment -> '.$notifycomment->id );
}

// ==============================================


// ==============================================
// ==============================================
// ==============================================
// ==============================================


// ==============================================
function luxcouriergateway_cron_minute_clear_orderinprocess()
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    $maxstart = time() - 1800;

    global $wpdb;
    $wpdb->query( ' DELETE FROM ' . $wpdb->prefix.'luxcouriergateway_orderinprocess WHERE `start` <= '.$maxstart.' ; ' );
    luxcouriergateway_savelog( __FUNCTION__, 'e' );
}

// ==============================================



// ==============================================
// ==============================================
// ==============================================
// ==============================================




// ==============================================
function luxcouriergateway_cron_minute_process_order_notify()
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    global $wpdb;
    $all = $wpdb->get_results( " SELECT * FROM ".$wpdb->prefix."luxcouriergateway_notifyorder WHERE `nexttentative` <= ".time()." ORDER BY `nexttentative`, `id` LIMIT 2 ; " );

    $LUXCOURIERGATEWAYTOOLSOBJ = new LUXCOURIERGATEWAYTOOLS();
    
    foreach ($all as $v)
    {
        $inprocess = luxcouriergateway_cron_minute_process_order_notify_is_inprocess($v->idorder);
        if ($inprocess) continue;

        luxcouriergateway_cron_minute_process_order_notify_set_inprocess($v->idorder);

        $order = LUXCOURIERGATEWAY_PLUGINTOOLS::getOrderDetailById($v->idorder);
        luxcouriergateway_cron_minute_process_order($order, $v, $LUXCOURIERGATEWAYTOOLSOBJ);

        luxcouriergateway_cron_minute_process_order_notify_close_inprocess($v->idorder);
    }

    $LUXCOURIERGATEWAYTOOLSOBJ->logoutall();
    
    luxcouriergateway_savelog( __FUNCTION__, 'e' );
}

// ==============================================
function luxcouriergateway_cron_minute_get_orderinfo_obj($idorder)
{
    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder );
    global $wpdb;
    $all = $wpdb->get_results( " SELECT * FROM ".$wpdb->prefix."luxcouriergateway_orderinfo WHERE `idorder` = ".$idorder." LIMIT 1 ; " );

    $rez = false;
    if (count($all))
    {
        $rez = reset($all);
    }
    
    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder );
    return $rez;
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_saveorderinfo($idorder, $idluxcourierorder)
{
    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder.'; idluxcourierorder -> '.$idluxcourierorder );
    $tobj = luxcouriergateway_cron_minute_get_orderinfo_obj($idorder);
    if ($tobj) return $tobj;

    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_orderinfo', array( 'idorder' => $idorder, 'idluxcourierorder' => $idluxcourierorder ) );

    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder.'; idluxcourierorder -> '.$idluxcourierorder );
    return luxcouriergateway_cron_minute_get_orderinfo_obj($idorder);
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_notify_is_inprocess($idorder)
{
    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder );
    global $wpdb;
    $all = $wpdb->get_results( " SELECT * FROM ".$wpdb->prefix."luxcouriergateway_orderinprocess WHERE `idorder` = ".$idorder." LIMIT 1 ; " );
    
    if (count($all)) return true;
    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder );
    return false;
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_notify_set_inprocess($idorder)
{
    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder );
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_orderinprocess', array( 'idorder' => $idorder, 'start' => time() ) );
    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder );
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_notify_close_inprocess($idorder)
{
    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$idorder );
    global $wpdb;
    $wpdb->delete( $wpdb->prefix.'luxcouriergateway_orderinprocess', array( 'idorder' => $idorder ) );
    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$idorder );
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_setprocesedobj($notifyorder)
{
    luxcouriergateway_savelog( __FUNCTION__, 's', 'idorder -> '.$notifyorder->idorder );
    global $wpdb;
    $wpdb->delete( $wpdb->prefix.'luxcouriergateway_notifyorder', array( 'idorder' => $notifyorder->idorder ) );
    luxcouriergateway_savelog( __FUNCTION__, 'e', 'idorder -> '.$notifyorder->idorder );
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_erroronprocess($notifyorder)
{
    luxcouriergateway_savelog( __FUNCTION__, 's', 'id -> '.$notifyorder->id );
    $notifyorder->tentatives++;
    $delay = $notifyorder->tentatives * 300;
    $notifyorder->nexttentative = time() + $delay;

    global $wpdb;
    if ($notifyorder->tentatives > 3)
    {
        $wpdb->delete( $wpdb->prefix.'luxcouriergateway_notifyorder', array( 'id' => $notifyorder->id ) );
    } else
    {
        $wpdb->update( $wpdb->prefix.'luxcouriergateway_notifyorder', array('tentatives' => $notifyorder->tentatives, 'nexttentative' => $notifyorder->nexttentative), array( 'id' => $notifyorder->id ) );
    }
    luxcouriergateway_savelog( __FUNCTION__, 'e', 'id -> '.$notifyorder->id );
}

// ==============================================
function luxcouriergateway_cron_minute_process_order($order, $notifyorder, $LUXCOURIERGATEWAYTOOLSOBJ)
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    $t = luxcouriergateway_cron_minute_process_order_send($order, $LUXCOURIERGATEWAYTOOLSOBJ);
    if ($t)
    {
        luxcouriergateway_cron_minute_process_order_setprocesedobj($notifyorder);
        return;
    } 
    
    luxcouriergateway_cron_minute_process_order_erroronprocess($notifyorder);
    luxcouriergateway_savelog( __FUNCTION__, 'e' );
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order)
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    $tobj = (array)$order['shipping_lines'];
    $tobj = reset($tobj);

    if (!is_array($tobj)) return false;
    if (!isset($tobj['method_id'])) return false;

    luxcouriergateway_savelog( __FUNCTION__, 'e' );
    return $tobj['method_id'];
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_send_check_iscorrect_shipping_method($order)
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    if (!is_array($order)) return false;
    if (!isset($order['id'])) return false;
    if (!$order['id']) return false;

    $method_id = luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order);

    if (!$method_id) return false;
    if (strpos($method_id,'LUXCOURIERGATEWAY_') === false) return false;

    
    $toptions = get_option('woocommerce_'.$method_id.'_settings');
    if (!is_array($toptions)) return false;
    if (!isset($toptions['enabled'])) return false;
    if ($toptions['enabled'] != 'yes') return false;

    luxcouriergateway_savelog( __FUNCTION__, 'e' );
    return true;
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_send_checkforcancel($order, $LUXCOURIERGATEWAYTOOLSOBJ, $orderinfo)
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    $needinfo = false;
    if (!$needinfo && !$order['status']) $needinfo = true;
    if (!$needinfo && in_array($order['status'], array('cancelled', 'refunded', 'failed'))) $needinfo = true;

    // --------------------
    if (!$needinfo) return false;
    if (!$orderinfo) return true;

    $tmp = $LUXCOURIERGATEWAYTOOLSOBJ->cancelorder(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order), $orderinfo->idluxcourierorder);
    luxcouriergateway_savelog( __FUNCTION__, 'e' );
    return true;
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_send_checkforarchive($order, $LUXCOURIERGATEWAYTOOLSOBJ, $orderinfo)
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    $needinfo = false;
    if (!$needinfo && in_array($order['status'], array('completed'))) $needinfo = true;

    // --------------------

    if (!$needinfo) return false;
    if (!$orderinfo) return false;

    $tmp = $LUXCOURIERGATEWAYTOOLSOBJ->archiveorder(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order), $orderinfo->idluxcourierorder);
    luxcouriergateway_savelog( __FUNCTION__, 'e' );
    return true;
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_send_checkforreadyfortake($order, $LUXCOURIERGATEWAYTOOLSOBJ, $orderinfo)
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    if (!$orderinfo || !$orderinfo->id) return false;

    $needinfo = false;
    if (!$needinfo && in_array($order['status'], array('on-hold'))) $needinfo = true;

    // --------------------

    if (!$needinfo) return false;
    
    $tmp = $LUXCOURIERGATEWAYTOOLSOBJ->readyfortake(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order), $orderinfo->idluxcourierorder);

    luxcouriergateway_savelog( __FUNCTION__, 'e' );
    return true;
}

// ==============================================
function luxcouriergateway_cron_minute_process_order_getcreatepostdata($order, $LUXCOURIERGATEWAYTOOLSOBJ)
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    $arr = array();

    $arr['notifyurl'] = home_url().'/wp-content/plugins/'.LUXCOURIERGATEWAY_MAINDIR.'/extern-notify.php?idorder='.$order['id'];
    
    $arr['titular_name'] = trim($order['billing_address']['first_name'].' '.$order['billing_address']['last_name']);
    $arr['titular_phone'] = $order['billing_address']['phone'];
    $arr['titular_email'] = $order['billing_address']['email'];
    $arr['titular_exist_whatsapp'] = 0;
    $arr['titular_exist_viber'] = 0;
    $arr['titular_exist_telegram'] = 0;
    $arr['titular_contacte'] = '';

    $arr['destinatar_name'] = trim($order['shipping_address']['first_name'].' '.$order['shipping_address']['last_name']);
    $arr['destinatar_phone'] = $order['billing_address']['phone'];
    $arr['destinatar_contacte'] = '';

    $arr['colet_descriere'] = home_url().' Order: '.$order['id'];
    $arr['colet_bucati'] = 1;
    
    $arr['idpaymethod'] = 0;
    $arr['price_client'] = 0;


    if (
            isset($order['payment_details'])
            && isset($order['payment_details']['method_id'])
            && strtolower($order['payment_details']['method_id']) == 'cod'
        )
        {
            // plata la livrare
            $lcpaymethods = $LUXCOURIERGATEWAYTOOLSOBJ->getpaymethods(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order));
            $lcpaymethods = (array)$lcpaymethods;
            if (!isset($lcpaymethods['objects'])) $lcpaymethods['objects'] = array();
            $lcpaymethods = (array)$lcpaymethods['objects'];
            
            foreach ((array)$lcpaymethods as $v)
            {
                if (!$v['platalalivrare']) continue;
                if ($v['plataposterminal']) continue;

                
                $arr['idpaymethod'] = $v['id'];
                break;
            }

            if ($arr['idpaymethod'])
            {
                $arr['price_client'] = $order['total'];
            }
        }

    $arr['typedelivery'] = LUXCOURIERGATEWAY_TYPEDELIVERY::GET_TYPEDELIVERY(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order));
    
    $arr['addresses'] = array();
    
    // ---------------------------------
    $toptions = get_option('woocommerce_'.luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order).'_settings');

    $taddress = array();
    $taddress['adreesstype'] = LUXCOURIERGATEWAY_ADDRESSTYPE::START_ADDRESS;
    $taddress['addressorder'] = 1;
    $taddress['country'] = $order['shipping_address']['country'];
    $taddress['address'] = $toptions['start_address'];
    $taddress['address_coment'] = '';
    $taddress['time'] = $order['delivery_data']['delivery_time'];
    $taddress['time_interval'] = $order['delivery_data']['delivery_interval'];
    $taddress['time_interval_isfixed'] = 0;
    $taddress['date_urgent'] = 0;

    $arr['addresses'][] = $taddress;
    unset($taddress);
    // ---------------------------------
    
    // ---------------------------------
    $taddress = array();
    $taddress['adreesstype'] = LUXCOURIERGATEWAY_ADDRESSTYPE::END_ADDRESS;
    $taddress['addressorder'] = 100;
    $taddress['country'] = $order['shipping_address']['country'];
    $taddress['address'] = 
            $order['shipping_address']['address_1']
            .', '.$order['shipping_address']['address_2']
            .', '.$order['shipping_address']['city']
            .', '.$order['shipping_address']['state']
            .', '.$order['shipping_address']['formated_state']
            .', '.$order['shipping_address']['country']
            .', '.$order['shipping_address']['formated_country']
        ;
    $taddress['address_coment'] = '';
    $taddress['time'] = $order['delivery_data']['delivery_time'];
    $taddress['time_interval'] = $order['delivery_data']['delivery_interval'];
    $taddress['time_interval_isfixed'] = 0;
    $taddress['date_urgent'] = 0;

    $arr['addresses'][] = $taddress;
    unset($taddress);
    // ---------------------------------

    // ---------------------------------
    $arr['imageurl'] = '';
    $arr['imageurls'] = array();
    foreach ($order['line_items'] as $v)
    {
        $arr['colet_descriere'] .= "\n".'#'.$v['product_id'].'-'.$v['name'].' - '.$v['quantity'];

        if ($v['product_thumbnail_url'])
        {
            if (!$arr['imageurl']) $arr['imageurl'] = $v['product_thumbnail_url'];
            if ($arr['imageurl'] != $v['product_thumbnail_url']) $arr['imageurls'][] = $v['product_thumbnail_url'];
        }
    }
    // ---------------------------------

    luxcouriergateway_savelog( __FUNCTION__, 'e' );
    return $arr;
}	

// ==============================================
function luxcouriergateway_cron_minute_process_order_send($order, $LUXCOURIERGATEWAYTOOLSOBJ)
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    // -----------------------------------------------
    $t = luxcouriergateway_cron_minute_process_order_send_check_iscorrect_shipping_method($order);
    if (!$t) return true;
    // -----------------------------------------------

    $orderinfo = luxcouriergateway_cron_minute_get_orderinfo_obj($order['id']);

    $t = $LUXCOURIERGATEWAYTOOLSOBJ->checkcompanydetail(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order));
    
    // -----------------------------------------------
    $t = luxcouriergateway_cron_minute_process_order_send_checkforcancel($order, $LUXCOURIERGATEWAYTOOLSOBJ, $orderinfo);
    if ($t) return true;
    // -----------------------------------------------

    // -----------------------------------------------
    $t = luxcouriergateway_cron_minute_process_order_send_checkforarchive($order, $LUXCOURIERGATEWAYTOOLSOBJ, $orderinfo);
    if ($t) return true;
    // -----------------------------------------------
    
    if ($orderinfo && $orderinfo->id && $orderinfo->idluxcourierorder)
    {
        // modificari
        $postdata = luxcouriergateway_cron_minute_process_order_getcreatepostdata($order, $LUXCOURIERGATEWAYTOOLSOBJ);
        $postdata['idorder'] = $orderinfo->idluxcourierorder;

        $tmp = $LUXCOURIERGATEWAYTOOLSOBJ->changeorder(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order), $postdata);

        if ($tmp === false) return false;
    } else
    {
        // creare
        $postdata = luxcouriergateway_cron_minute_process_order_getcreatepostdata($order, $LUXCOURIERGATEWAYTOOLSOBJ);

        $tmp = $LUXCOURIERGATEWAYTOOLSOBJ->savepreorder(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order), $postdata);

        if ($tmp === false) return false;

        $orderinfo = luxcouriergateway_cron_minute_process_order_saveorderinfo($order['id'], $tmp['obj']['id']);

        $tmp = $LUXCOURIERGATEWAYTOOLSOBJ->confirmorder(luxcouriergateway_cron_minute_process_order_get_shippingmethod_id($order), $orderinfo->idluxcourierorder);

        if ($tmp === false) return false;

        $orderinfo = luxcouriergateway_cron_minute_process_order_saveorderinfo($order['id'], $tmp['obj']['id']);
    }

    // -----------------------------------------------
    luxcouriergateway_cron_minute_process_order_send_checkforreadyfortake($order, $LUXCOURIERGATEWAYTOOLSOBJ, $orderinfo);
    // -----------------------------------------------

    luxcouriergateway_savelog( __FUNCTION__, 'e' );
    return true;
}

// ==============================================

// ==============================================
// ==============================================
// ==============================================
// ==============================================



// ==============================================
function luxcouriergateway_savelog( $funct = '', $tip = '', $note = '' )
{
    $data = time();
    global $wpdb;
    $wpdb->insert( $wpdb->prefix.'luxcouriergateway_logs', array( 'data' => $data, 'funct' => $funct, 'tip' => $tip, 'note' => $note ) );
}

// ==============================================
function luxcouriergateway_cron_minute_clear_orderlogs()
{
    luxcouriergateway_savelog( __FUNCTION__, 's' );
    $maxstart = time() - 86400;

    global $wpdb;
    $wpdb->query( ' DELETE FROM ' . $wpdb->prefix.'luxcouriergateway_logs WHERE `data` <= '.$maxstart.' ; ' );
    luxcouriergateway_savelog( __FUNCTION__, 'e' );
}

// ==============================================

// ==============================================
// ==============================================
// ==============================================
// ==============================================




